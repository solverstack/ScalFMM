// --------------------------------
// See LICENCE file at project root
// File : scalfmm/lists/lists.hpp
// --------------------------------
#pragma once

#include "scalfmm/lists/policies.hpp"
#include "scalfmm/lists/sequential.hpp"
#ifdef _OPENMP
#include "scalfmm/lists/omp.hpp"
#endif