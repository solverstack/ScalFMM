﻿// --------------------------------
// See LICENCE file at project root
// File : scalfmm/interpolation/uniform.hpp
// --------------------------------
#pragma once

#include "scalfmm/interpolation/uniform/uniform_interpolator.hpp"
#include "scalfmm/interpolation/uniform/uniform_storage.hpp"