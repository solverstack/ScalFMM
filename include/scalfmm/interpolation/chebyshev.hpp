// --------------------------------
// See LICENCE file at project root
// File : scalfmm/interpolation/chebyshev.hpp
// --------------------------------
#ifndef SCALFMM_INTERPOLATION_CHEBYSHEV_HPP
#define SCALFMM_INTERPOLATION_CHEBYSHEV_HPP

#include "scalfmm/interpolation/chebyshev/chebyshev_interpolator.hpp"

#endif   // SCALFMM_INTERPOLATION_UNIFORM_HPP
