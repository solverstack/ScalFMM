// --------------------------------
// See LICENCE file at project root
// File : scalfmm/interpolation/traits.hpp
// --------------------------------
#pragma once

namespace scalfmm::interpolation
{
    /**
     * @brief
     *
     * @tparam Derived
     */
    template<typename Derived>
    struct interpolator_traits;
}   // namespace scalfmm::interpolation
